# MEDcury Assignment
### 1st Question : Write code to solve this solution.
A phone book is backed up as a text file. On each line of the file there’s the phone number (formatted as +X-abc-def-ghij where X stands for  one or two digits), the corresponding name between “ ” and “ ” and the address. 
Unfortunately everything is mixed, things are not always in the same order;
parts of lines are cluttered with non-alpha-numeric characters  (except inside phone number and name). 

**Examples of a phone book lines**

> "/+1-541-754-3010 156 Alphand_St. <J Steeve>\n" 
" 133, Green, Rd. <E Kustur> NY-56423 ;+1-541-914-3010!\n" 
"<Anastasia> +48-421-674-8974 Via Quirinal Roma\n" 

Given the lines of his phone book and a phone number returns a string for this number : "Phone => num, Name => name, Address =>  adress"

**Examples:**
> s = "/+1-541-754-3010 156 Alphand_St. <J Steeve>\n 133, Green, Rd. <E Kustur> NY-56423 ;+1-541-914-3010!\n" phone(s, "1-541-754-3010") 

should return "Phone => 1-541-754-3010, Name => J Steeve, Address => 156 Alphand St." It can happen that, for a few phone numbers, 
there are many people for the same phone number ( number ), then your code will  return : "Error => Too many people: number" 
<Peter O'Brien> High Street +1-908-512-2222; CC-47209\n <Anastasia> +48-421-674-8974 Via Quirinal Roma\n <P  Salinger> Main Street, +1-098-512-2222, Denver\n" 
or it can happen that the number number is not in the phone book, in that case
return: "Error => Not found: number" 

### 2st Question : WUP from string
**Example :**  
>songDecoder("WUBWEWUBAREWUBWUBTHEWUBBACKYARDWUBMYWUBFRIENDWUB") 

=> WE ARE THE BACKYARD MY FRIEND 

---

# How to Run 
If you're already install Node.js or Yarn Skipped to Step 2

### Step 1: Download Node.js Installer ( recommend version 10+ )
-> Download or install at https://nodejs.org/en/

### Step 2: Install requirement package
-> Go to root of project directory (../medcury-assignment) via Terminal Or CMD 
-> type "npm install" or "yarn install"

### Step 3: Run this project
-> type "npm run test" or "yarn run test"