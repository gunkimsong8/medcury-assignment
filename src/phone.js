const phoneRegex = new RegExp(/(?<=\+)[0-9]{1,2}[-][0-9]{3}[-][0-9]{3}[-][0-9]{4}/, 'g')
const nameRegex = new RegExp(/(?<=\<)[A-z']* [A-z']*(?=>)|(?<=\<)[A-z']*(?=>)/,'g')
const addressRegex = new RegExp(/[<>+?!$*,;://]|[ ][_]|$([  ] )/,'g')
const underlineRegex = new RegExp(/(?<=[A-z])_|(?<=[ ])_|^_|_$/,'g')
const headAndTailSpaceBarRegex = new RegExp(/^\s+|\s+$/,'g')
const middleSpaceBarRegex = new RegExp(/\s\s+/,'g')

function phone(string,number){
    let lists = []
    let mixedPhoneBooksLists = string.split('\n')
    mixedPhoneBooksLists.forEach(item => {
        if(item){
            let name = item.match(nameRegex).toString()
            let phone = item.match(phoneRegex).toString()
            let address = item.replace(nameRegex,'')
                              .replace(phoneRegex,'')
                              .replace(addressRegex,'')
                              .replace(underlineRegex,' ')
                              .replace(headAndTailSpaceBarRegex,'')
                              .replace(middleSpaceBarRegex,' ')
            lists.push({
                name : name,
                phone : phone,
                address, address
            })
        }
    });

    let results = lists.filter(item => item.phone == number)
    if(results.length === 1) return `Phone => ${results[0].phone}, Name => ${results[0].name}, Address => ${results[0].address}`
    if(results.length > 1 ) return `Error => Too many people: ${number}`
    if(!results.length)  return `Error => Not found: ${number}`
}

module.exports = phone