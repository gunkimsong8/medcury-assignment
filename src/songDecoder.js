function songDecoder(song) {
    return song.replace(/WUB/g,' ')
    .replace(/  +/g,' ')
    .replace(/^ +/g,'')
    .replace(/ $/g,'')
}

module.exports = songDecoder