const songDecoder = require('../src/songDecoder')

describe("phone",() => { 
    test("WUB should be replaced by 1 space",()=> { 
        expect(songDecoder("AWUBBWUBC")).toEqual("A B C")
    })
    test("multiples WUB should be replaced by only 1 space",()=> { 
        expect(songDecoder("AWUBWUBWUBBWUBWUBWUBC")).toEqual("A B C")
    })
    test("heading or trailing spaces should be removed",()=> { 
        expect(songDecoder("WUBAWUBBWUBCWUB")).toEqual("A B C")
    })
    test("example test case",()=>{
        expect(songDecoder("WUBWEWUBAREWUBWUBTHEWUBBACKYARDWUBMYWUBFRIENDWUB")).toEqual("WE ARE THE BACKYARD MY FRIEND")
    })
})